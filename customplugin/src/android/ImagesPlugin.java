package com.openworldcrafter;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.net.Uri;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

public class ImagesPlugin extends CordovaPlugin {
    private ImageView imageView;

    @Override
    public boolean execute(final String action, final JSONArray inputs, final CallbackContext callbackContext) throws JSONException {
        if("showImage".equals(action)) {
            final String url = inputs.getJSONObject(0).getString("url");

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final ImagesPlugin pl = ImagesPlugin.this;

                    if(pl.imageView == null) {
                        pl.imageView = new ImageView(pl.cordova.getActivity());
                        pl.imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(pl.imageView.getParent() != null) {
                                    ((ViewGroup) pl.imageView.getParent()).removeView(pl.imageView);
                                }
                            }
                        });
                        pl.imageView.setBackgroundColor(Color.rgb(0, 0, 0));
                    }

                    if(pl.imageView.getParent() != null) {
                        ((ViewGroup) pl.imageView.getParent()).removeView(pl.imageView);
                    }

                    pl.imageView.setImageURI(Uri.parse(url));
                    pl.cordova.getActivity().addContentView(pl.imageView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                }
            });
        }

        return true;
    }
}
