# openworldcrafter mobile

Made using Apache Cordova with the same codebase as the desktop version. This
repository will need to be cloned as a sibling directory to the core
`openworldcrafter` repository, and both will need to have the same names as on
GitHub. This repository only contains mobile-specific stuff, and the build
script copies the actual JS code from the core repository. As a result, updates
to openworldcrafter should not require updates here (unless new native
integrations are needed).

Cordova doesn't seem to be very VCS-friendly, so I'm honestly not really sure
what you'll need to do to get it running on your own computer. It requires an
empty directory to create a new project, and I've removed most of what makes
this directory a Cordova project because 1) it's redundant, and 2) it contains
file paths and stuff that are specific to my computer.

As a result, you might have to do some gymnastics to get it working properly.
