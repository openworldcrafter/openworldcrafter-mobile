"use strict"

/* Helper functions for Cordova File Plugin. */
;(() => {
    const noop = () => {}

    var fs = {}
    fs.readFile = function(filename, cb) {
        var filepath = cordova.file.dataDirectory + filename

        window.resolveLocalFileSystemURL(filepath, fileEntry => {
            fileEntry.file(file => {
                var reader = new FileReader()

                // no, this can't be an arrow function. >:-(
                reader.onloadend = function() {
                    (cb || noop)(undefined, this.result)
                }

                reader.readAsText(file)
            }, cb || noop)
        }, cb || noop)
    }

    function readFileArrayBuffer(filename, cb) {
        var filepath = cordova.file.dataDirectory + filename

        window.resolveLocalFileSystemURL(filepath, fileEntry => {
            fileEntry.file(file => {
                var reader = new FileReader()

                // no, this can't be an arrow function. >:-(
                reader.onloadend = function() {
                    (cb || noop)(undefined, this.result)
                }

                reader.readAsArrayBuffer(file)
            }, cb || noop)
        }, cb || noop)
    }
    fs.readFileArrayBuffer = readFileArrayBuffer

    fs.writeFileJSON = function(filename, data, cb) {
        fs.writeFile(filename, JSON.stringify(data), cb)
    }

    fs.writeFile = function(filename, string, cb) {
        var blob = new Blob([string], { type: "text/plain" })
        fs.writeFileBlob(filename, blob, cb)
    }

    /* Writes to a file, creating it if it does not exist. */
    fs.writeFileBlob = function(filename, blob, cb) {
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, dirEntry => {
            dirEntry.getFile(filename, { create: true, exclusive: false }, fileEntry => {
                fileEntry.createWriter(writer => {
                    writer.onwriteend = e => (cb || noop)(undefined, "success")
                    writer.onerror = e => (cb || noop)(e)

                    writer.write(blob)
                }, cb || noop)
            }, cb || noop)
        }, cb || noop)
    }

    /* Writes to a file, creating it if it does not exist. */
    // TODO: for testing, remove
    fs.writeFileBlobExternal = function(filename, blob, cb) {
        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, dirEntry => {
            dirEntry.getFile(filename, { create: true, exclusive: false }, fileEntry => {
                console.log("output file", fileEntry);
                fileEntry.createWriter(writer => {
                    writer.onwriteend = e => (cb || noop)(undefined, "success")
                    writer.onerror = e => (cb || noop)(e)

                    writer.write(blob)
                }, cb || noop)
            }, cb || noop)
        }, cb || noop)
    }

    fs.deleteFile = function(file, cb) {
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory + file, fileEntry => {
            fileEntry.remove(cb)
        }, cb)
    };

    fs.getURL = function(file, cb) {
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory + file, fileEntry => {
            cb(undefined, fileEntry.toURL())
        }, cb)
    }

    fs.mkdir_safe = function(name, cb) {
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, parentEntry => {
            parentEntry.getDirectory(name, { create: true, exclusive: false }, dirEntry => {
                (cb || noop)(undefined, "success")
            }, cb || noop)
        }, cb || noop)
    }

    fs.deleteDirectory = function(dir, cb) {
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory + dir, dirEntry => {
            dirEntry.removeRecursively(() => {
                cb()
            }, cb || noop)
        }, cb || noop)
    }

    function readdir(dir, cb) {
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory + dir, dirEntry => {
            var reader = dirEntry.createReader()
            reader.readEntries(entries => {
                (cb || noop)(undefined, entries)
            }, cb || noop)
        }, cb || noop)
    }
    fs.readdir = readdir

    /*
        note that this is a breadth-first search. also, it does not list directories
    */
    function readdirRecursive(dir, cb, list) {
        if(!Array.isArray(dir)) dir = [dir]
        if(!list) list = []

        if(!dir.length) cb(undefined, list)
        else {
            readdir(dir[0], (err, entries) => {
                if(err) cb(err)
                else {
                    for(var entry of entries) {
                        var name = dir[0] + "/" + entry.name

                        if(entry.isDirectory) dir.push(name)
                        else list.push(name)
                    }

                    // remove the directory we just read
                    dir.shift()
                    // now keep going down that list
                    readdirRecursive(dir, cb, list)
                }
            })
        }
    }
    fs.readdirRecursive = readdirRecursive

    // fs.makeZipFile = function(dir, cb) {
    //     var zip = new jszip()
    //
    //     readdirRecursive(dir, (err, list) => {
    //         if(err) {
    //             cb(err)
    //             return
    //         }
    //         var remaining = list.length
    //
    //         for(var entry of list) {
    //             readFileArrayBuffer(entry, (err, buffer) => {
    //                 if(err) {
    //                     // make sure the callback isn't called again
    //                     remaining = -1
    //                     cb(err)
    //                 } else {
    //                     zip.file(entry, buffer)
    //                     remaining --
    //
    //                     if(remaining === 0) {
    //                         console.log("creating zip file")
    //                         zip.generateAsync({ type: "blob" }).then(file => cb(undefined, file))
    //                     }
    //                 }
    //             })
    //         }
    //     })
    // }

    window.$native = window.$native || {}
    window.$native.fs = fs
})()

;(() => {
    // load cordova.js
    var cordovaScript = document.createElement("script")
    cordovaScript.src = "../cordova.js"
    cordovaScript.addEventListener("load", start)
    document.body.appendChild(cordovaScript)

    function start() {
        document.addEventListener('deviceready', function() {
            console.log("Device is ready")

            window.$images = {
                showImage: function(url) {
                    cordova.exec(console.log, console.log, "Images", "showImage", [{ url }])
                }
            }

            // run the code that loads the page
            var script = document.createElement("script")
            script.src = "../js/bundle.js"
            script.addEventListener("load", $go)
            document.body.appendChild(script)
        }, false)
    }
})()
