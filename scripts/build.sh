#!/bin/bash

# Start with a clean slate
rm -rf www
mkdir www
rm -rf ../openworldcrafter/dist/app

# Go to the core repo and run our mobile build
cd ../openworldcrafter

# do this before checking git status to ensure consistency
NODE_ENV=production yarn run compile-all

# check for uncommitted changes
if [[ -n $(git status -s) ]]; then
    echo "There are uncomitted changes! Please commit or stash them before building for production. All production builds must be compiled from committed code."
else
    echo "Compiling mobile from build $(git rev-parse HEAD)"

    OWC_ORIGIN="https://online.openworldcrafter.com" NODE_ENV=production OWC_COMMIT_ID=$(git rev-parse HEAD) yarn run compile-app

    cd ../openworldcrafter-mobile

    # Copy native.js and placeholder.js into the www directory
    mkdir www/js
    cp native.js www/js/native.js
    cp ../openworldcrafter/js/placeholder.js www/js/placeholder.js

    # Copy over the build
    cp -r ../openworldcrafter/dist/app/* www

    # Make sure we're using the production app ID
    ./scripts/setpackageid.js

    # Create the build
    if [[ "$1" = "--sign" ]]; then
        cordova build --release -- --keystore="~/.keystore" --alias="openworldcrafter@gmail.com"
    else
        cordova build --release
    fi
    # Calculate SHA/MD5 hashes and PGP signatures, if asked to do so
    # if [[ "$1" = "--sign" ]]; then
    # fi
fi
