#!/usr/bin/env node

"use strict"

// this code is 💩 but ¯\_(ツ)_/¯
// it works by search-and-replacing the package id in config.xml

const fs = require('fs')

var config = fs.readFileSync("config.xml", "utf-8")
// quick and dirty replace
var newPackageId = `"com.openworldcrafter.mobile${ process.argv[2] || "" }"`
config = config.replace("\"com.openworldcrafter.mobile\"", newPackageId)
config = config.replace("\"com.openworldcrafter.mobile.dev\"", newPackageId)
fs.writeFileSync("config.xml", config)
console.log("Building " + newPackageId)
